package com.gitee.sunchenbin.mybatis.actable.constants;

public class Constants {

	public static final String DATABASE_TYPE_KEY 		= "mybatis.database.type";
	public static final String MODEL_PACK_KEY 			= "mybatis.model.pack";
	public static final String TABLE_AUTO_KEY 			= "mybatis.table.auto";
	
	public static final String DATABASE_TYPE_KEY_VALUE 	= "${mybatis.database.type:NULL}";
	public static final String MODEL_PACK_KEY_VALUE 	= "${mybatis.model.pack:NULL}";
	public static final String TABLE_AUTO_KEY_VALUE 	= "${mybatis.table.auto:NULL}";
	
	public static final String NULL 					= "NULL";
	
}
